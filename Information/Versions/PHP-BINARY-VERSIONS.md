# PHP BINARY VERSIONS

### 7.2

- [PHP-7.2-MacOS-x86_64.tar.gz](https://jenkins.pmmp.io/job/PHP-7.2-Aggregate/lastSuccessfulBuild/artifact/PHP-7.2-MacOS-x86_64.tar.gz)
- [PHP-7.2-Linux-x86_64.tar.gz](https://jenkins.pmmp.io/job/PHP-7.2-Aggregate/lastSuccessfulBuild/artifact/PHP-7.2-Linux-x86_64.tar.gz)
- [PHP-7.2-Windows-x64.zip](https://jenkins.pmmp.io/job/PHP-7.2-Aggregate/lastSuccessfulBuild/artifact/PHP-7.2-Windows-x64.zip)
