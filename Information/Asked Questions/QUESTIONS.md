# Frequently Asked Questions

### How Can I Install The Latest PHP Version?

- Download the PHP binary for your OS.

- Delete the bin directory in your server folder.

- Extract the new PHP binary. You should see a new bin directory has been created.

- Put the bin folder in the folder you want to put it (i don't know what folder it is)

### Where Can I Get The Latest PHP Version?

__Here Are The Downloads For The Latest PHP Version:__

For Linux
- [PHP-7.2-Linux-x86_64.tar.gz](https://jenkins.pmmp.io/job/PHP-7.2-Aggregate/lastSuccessfulBuild/artifact/PHP-7.2-Linux-x86_64.tar.gz)

For MacOS
- [PHP-7.2-MacOS-x86_64.tar.gz](https://jenkins.pmmp.io/job/PHP-7.2-Aggregate/lastSuccessfulBuild/artifact/PHP-7.2-MacOS-x86_64.tar.gz)

For Windows
- [PHP-7.2-Windows-x64.zip](https://jenkins.pmmp.io/job/PHP-7.2-Aggregate/lastSuccessfulBuild/artifact/PHP-7.2-Windows-x64.zip)

- Or My GitHub Repository [PHP Versions File](https://github.com/Official-Developers/PocketMine-MP/blob/master/Information/Versions/PHP-BINARY-VERSIONS.md)

### How Can I Install The Latest PocketMine-MP.phar Version?

- Create a new directory for PocketMine-MP.

- Download PocketMine-MP.phar

- Rename the .phar to PocketMine-MP.phar.

- Place it in the PocketMine-MP directory you just created.

### Where Can I Get The Latest PocketMine-MP.phar File?

- [Downloads](https://github.com/pmmp/PocketMine-MP/releases) 

- Or My GiHub Repository [PocketMine-MP Versions File](https://github.com/Official-Developers/PocketMine-MP/blob/master/Information/Versions/VERSIONS.md)

### How Can I Install Plugins?

- Copy the downloaded file to your server's plugins folder.
- Run stop on your server, then start it again.

### Where Can I Get The Plugins?

- [Poggit](https://poggit.pmmp.io/plugins)
